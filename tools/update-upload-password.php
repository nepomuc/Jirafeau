<?php
/*
 *  Jirafeau, your web file repository
 *  Copyright (C) 2008  Julien "axolotl" BERNARD <axolotl@magieeternelle.org>
 *  Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
define('JIRAFEAU_ROOT', dirname(__FILE__) . '/../');
define('JIRAFEAU_CFG', JIRAFEAU_ROOT . 'lib/config.local.php');
define('JIRAFEAU_UPLOAD_PASSWORD', JIRAFEAU_ROOT . 'tools/upload-password.txt');

require(JIRAFEAU_ROOT . 'lib/settings.php');
require(JIRAFEAU_ROOT . 'lib/functions.php');
require(JIRAFEAU_ROOT . 'lib/lang.php');

if (file_exists(JIRAFEAU_CFG)) {
    require(JIRAFEAU_CFG);
    $password_generated = jirafeau_random_password(8);
    $cfg['upload_password']['from-update-upload-password-script'] = hash('sha256', $password_generated);
    jirafeau_export_cfg($cfg);
    $fileWrite = file_put_contents(JIRAFEAU_UPLOAD_PASSWORD, $password_generated);
    if (false === $fileWrite) {
        jirafeau_fatal_error(t('Can not write password file.'));
    }
}
else {
    echo 'No config file found.';
}


?>